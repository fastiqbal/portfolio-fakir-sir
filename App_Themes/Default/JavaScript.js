function LeftMenuEffect()
{
    $("ul.menu_body li:even").each(function(){   
        $(this).css({ "background-color": "#362f2d" });
    });
	
	
	$('div.menu_head').each(function(){
	    $(this).click(function () {
	    $(this).next().slideToggle('medium');
        });
	});
	
	$('ul.menu_body li a').mouseover(function () {
	    $(this).animate({ fontSize: "18px", paddingLeft: "20px" }, 50 );
    });
	
	$('ul.menu_body li a').mouseout(function () {
	    $(this).animate({ fontSize: "14px", paddingLeft: "10px" }, 50 );
    });
}
function topMenuEffect()
{
    stuHover = function() {
        var cssRule;
        var newSelector;
        for (var i = 0; i < document.styleSheets.length; i++)
            for (var x = 0; x < document.styleSheets[i].rules.length ; x++)
            {
                cssRule = document.styleSheets[i].rules[x];
                if (cssRule.selectorText.indexOf("LI:hover") != -1)
                {
                    newSelector = cssRule.selectorText.replace(/LI:hover/gi, "LI.iehover");
                    document.styleSheets[i].addRule(newSelector , cssRule.style.cssText);
                }
            }
            var getElm = document.getElementById("nav").getElementsByTagName("LI");
            for (var i=0; i<getElm.length; i++)
            {
                getElm[i].onmouseover=function()
                {
                    this.className+=" iehover";
                }
                getElm[i].onmouseout=function() {
                this.className=this.className.replace(new RegExp(" iehover\\b"), "");
            }
        }
    }
    if (window.attachEvent) window.attachEvent("onload", stuHover);
}

$(document).ready(function() {
    topMenuEffect();
    LeftMenuEffect();
});

